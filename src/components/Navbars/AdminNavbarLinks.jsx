import React, { Component } from "react";
import {
  Nav,
} from "react-bootstrap";
import Button from "components/CustomButton/CustomButton.jsx";
import { hashHistory } from 'react-router';
class HeaderLinks extends Component {
  logout() {
    let token = localStorage.getItem('token')
    console.log('token', token)
    fetch('http://93.188.167.68:3001/api/' + 'users/logout', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': token
      },
    }).then(response =>
      response.json()).then(response => {
        if (response.isSuccess == true) {
          console.log("Login:response", response)
          localStorage.removeItem('token')
          hashHistory.push('/auth/login-page')
        } else {
          alert(response.error);
        }
      }).catch(error => {
        console.log('Errorrrrrr:', error.message)
        alert(error.message)
        this.setState({
          isLoading: false
        })
      })
  }
  render() {
    return (
      <div>
        <Nav pullRight>
          <div className="text-danger">
            <Button
              onClick={() => this.logout()}
              bsStyle="danger"
              simple
              icon
            >
              <i className="pe-7s-close-circle" /> Log out
              </Button>{" "}
          </div>
        </Nav>
      </div>
    );
  }
}
export default HeaderLinks;
