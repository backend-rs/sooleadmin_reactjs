import Dashboard from "views/Dashboard.jsx";
import Users from "views/Users.jsx";
import Drives from "views/Drives.jsx";
import Rides from "views/Rides.jsx";
import LoginPage from "views/Pages/LoginPage.jsx";
import UpdateUserPage from "views/Pages/UpdateUser.jsx";
import OtpPage from "views/Pages/OtpPage.jsx";
import OtpVerifyPage from "views/Pages/OtpVerifyPage.jsx";
import ForgotPasswordPage from "views/Pages/ForgotPasswordPage.jsx";

var routes = [

  {
    path: "/dashboard",
    layout: "/admin",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard
  },
  {
    path: "/users",
    layout: "/admin",
    name: "Users",
    icon: "pe-7s-users",
    component: Users
  },
  {
    path: "/drives",
    layout: "/admin",
    name: "Drives",
    icon: "pe-7s-car",
    component: Drives
  },
  {
    redirect: true,
    path: "/edit-user",
    layout: "/admin",
    component: UpdateUserPage
  },
  {
    path: "/rides",
    layout: "/admin",
    name: "Rides",
    icon: "pe-7s-car",
    component: Rides
  },
  {
    redirect: true,
    path: "/login-page",
    layout: "/auth",
    name: "Login Page",
    mini: "LP",
    component: LoginPage
  }, {
    redirect: true,
    path: "/otp-page",
    layout: "/auth",
    name: "Otp Page",
    mini: "OTPP",
    component: OtpPage
  }, {
    redirect: true,
    path: "/verify-otp",
    layout: "/auth",
    name: "Otp Verify Page",
    mini: "OVP",
    component: OtpVerifyPage
  }, {
    redirect: true,
    path: "/forgot-password",
    layout: "/auth",
    name: "forgot password Page",
    mini: "Forgot",
    component: ForgotPasswordPage
  }
]
export default routes;
