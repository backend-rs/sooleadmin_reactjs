import React, { Component } from "react";
import { Grid, Col, Row } from "react-bootstrap";
import ChartistGraph from "react-chartist";
import Card from "components/Card/Card.jsx";
import StatsCard from "components/Card/StatsCard.jsx";
import LoadingOverlay from 'react-loading-overlay';
import {
  optionsBar,
  responsiveBar,
  table_data
} from "variables/Variables.jsx";
let token
let data = []

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counts: {},
      userData: {
        labels: [],
        series: [[]]
      },
      rideData: {
        labels: [],
        series: [[]]
      },
      driveData: {
        labels: [],
        series: [[]]
      },
      driverData: {
        labels: [],
        series: [[]]
      },
      isLoading: false
    };

  }


  componentDidMount() {
    let token = localStorage.getItem('token')
    console.log('token', token)
    this.setState({
      isLoading: true
    })
    fetch('http://93.188.167.68:3001/api/' + 'counts', {
      method: 'get',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': token
      },
    }).then(response =>
      response.json()).then(response => {
        // console.log("countsApi", response)
        if (response.isSuccess == true) {
          // console.log("countsApi:data", response.data.count)
          this.setState({
            counts: response.data.count
          })
        } else {
          alert(response.error);
        }
      }).catch(error => {
        console.log('Errorrrrrr:', error.message)
        alert(error.message)
        this.setState({
          isLoading: false
        })
      })
    this.getMonthlyUsersCount()
    this.getMonthlyRidesCount()
    this.getMonthlyDrivesCount()
    this.getMonthlyDriversCount()
  }
  getMonthlyUsersCount() {
    token = localStorage.getItem('token')
    console.log('token', token)
    fetch('http://93.188.167.68:3001/api/' + 'counts/monthly/users', {
      method: 'get',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': token
      },
    }).then(response =>
      response.json()).then(response => {
        if (response.isSuccess == true) {
          console.log("userMonthCountsApi:data", response.data)
          this.setState({
            userData: {
              labels: response.data.labels,
              series: [response.data.series]
            }
          })
          data.push({ userData: this.state.userData })
        } else {
          alert(response.error);
        }
      }).catch(error => {
        console.log('Errorrrrrr:', error.message)
        alert(error.message)
        this.setState({
          isLoading: false
        })
      })
  }
  getMonthlyRidesCount() {
    fetch('http://93.188.167.68:3001/api/' + 'counts/monthly/rides', {
      method: 'get',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': token
      },
    }).then(response =>
      response.json()).then(response => {
        if (response.isSuccess == true) {
          console.log("userMonthCountsApi:data", response.data)
          this.setState({
            rideData: {
              labels: response.data.labels,
              series: [response.data.series]
            }
          })
          data.push({ rideData: this.state.rideData })
        } else {
          alert(response.error);
        }
      }).catch(error => {
        console.log('Errorrrrrr:', error.message)
        alert(error.message)
        this.setState({
          isLoading: false
        })
      })
  }
  getMonthlyDrivesCount() {
    fetch('http://93.188.167.68:3001/api/' + 'counts/monthly/drives', {
      method: 'get',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': token
      },
    }).then(response =>
      response.json()).then(response => {
        if (response.isSuccess == true) {
          console.log("userMonthCountsApi:data", response.data)
          this.setState({
            driveData: {
              labels: response.data.labels,
              series: [response.data.series]
            }
          })
          data.push({ rideData: this.state.rideData })
        } else {
          alert(response.error);
        }
      }).catch(error => {
        console.log('Errorrrrrr:', error.message)
        alert(error.message)
        this.setState({
          isLoading: false
        })
      })
  }
  getMonthlyDriversCount() {
    fetch('http://93.188.167.68:3001/api/' + 'counts/monthly/drivers', {
      method: 'get',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': token
      },
    }).then(response =>
      response.json()).then(response => {
        if (response.isSuccess == true) {
          console.log("userMonthCountsApi:data", response.data)
          this.setState({
            isLoading: false,
            driverData: {
              labels: response.data.labels,
              series: [response.data.series]
            }
          })
          data.push({ rideData: this.state.rideData })
        } else {
          alert(response.error);
        }
      }).catch(error => {
        console.log('Errorrrrrr:', error.message)
        alert(error.message)
        this.setState({
          isLoading: false
        })
      })
  }
  createTableData() {
    var tableRows = [];
    for (var i = 0; i < table_data.length; i++) {
      tableRows.push(
        <tr key={i}>
          <td>
            <div className="flag">
              <img src={table_data[i].flag} alt="us_flag" />
            </div>
          </td>
          <td>{table_data[i].country}</td>
          <td className="text-right">{table_data[i].count}</td>
          <td className="text-right">{table_data[i].percentage}</td>
        </tr>
      );
    }
    return tableRows;
  }
  render() {
    // console.log('render', this.state.counts)
    return (
      <LoadingOverlay
        active={this.state.isLoading}
        spinner
        text='Loading, please wait...'
      >
        <div className="main-content">
          <Grid fluid>
            <Row>
              <Col lg={3} sm={6}>
                <StatsCard
                  bigIcon={<i className="pe-7s-users text-warning" />}
                  statsText="User"
                  statsValue={this.state.counts.users}
                  statsIcon={<i className="fa fa-refresh" />}
                  statsIconText="Updated now"
                />
              </Col>
              <Col lg={3} sm={6}>
                <StatsCard
                  bigIcon={<i className="pe-7s-users text-success" />}
                  statsText="Driver"
                  statsValue={this.state.counts.drivers}
                  statsIcon={<i className="fa fa-calendar-o" />}
                  statsIconText="Updated now"
                />
              </Col>
              <Col lg={3} sm={6}>
                <StatsCard
                  bigIcon={<i className="pe-7s-car text-danger" />}
                  statsText="Ride"
                  statsValue={this.state.counts.rides}
                  statsIcon={<i className="fa fa-clock-o" />}
                  statsIconText="Updated now"
                />
              </Col>
              <Col lg={3} sm={6}>
                <StatsCard
                  bigIcon={<i className="pe-7s-car text-info" />}
                  statsText="Drives"
                  statsValue={this.state.counts.drives}
                  statsIcon={<i className="fa fa-refresh" />}
                  statsIconText="Updated now"
                />
              </Col>
            </Row>
            <Row>

            </Row>
            <Row>
              <Col md={6}>
                <Card
                  title="Users"
                  category="Users connected with us this year"
                  content={
                    <ChartistGraph
                      data={this.state.userData}
                      type="Bar"
                      options={optionsBar}
                      responsiveOptions={responsiveBar}
                    />
                  }
                  stats={
                    <div>
                      <i className="fa fa-check" /> Data information certified
                  </div>
                  }
                />
              </Col>
              <Col md={6}>
                <Card
                  title="Drivers"
                  category="Drivers connected with us this year"
                  content={
                    <ChartistGraph
                      data={this.state.driverData}
                      type="Bar"
                      options={optionsBar}
                      responsiveOptions={responsiveBar}
                    />
                  }
                  stats={
                    <div>
                      <i className="fa fa-check" /> Data information certified
                  </div>
                  }
                />
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <Card
                  title="Rides"
                  category="Rides Per Month Completed"
                  content={
                    <ChartistGraph
                      data={this.state.rideData}
                      type="Bar"
                      options={optionsBar}
                      responsiveOptions={responsiveBar}
                    />
                  }
                  stats={
                    <div>
                      <i className="fa fa-check" /> Data information certified
                  </div>
                  }
                />
              </Col>
              <Col md={6}>
                <Card
                  title="Drives"
                  category="Drives completed by us this year"
                  content={
                    <ChartistGraph
                      data={this.state.driveData}
                      type="Bar"
                      options={optionsBar}
                      responsiveOptions={responsiveBar}
                    />
                  }
                  stats={
                    <div>
                      <i className="fa fa-check" /> Data information certified
                  </div>
                  }
                />
              </Col>
            </Row>
          </Grid>
        </div>
      </LoadingOverlay>
    );
  }
}

export default Dashboard;
