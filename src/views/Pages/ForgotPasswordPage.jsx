import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import { hashHistory } from 'react-router';

class ForgotPasswordPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: '',
      confirmPassword: '',
      cardHidden: true
    };
  }
  componentDidMount() {
    setTimeout(
      function () {
        this.setState({ cardHidden: false });
      }.bind(this),
      700
    );
  }

  setPassword = () => {
    if (!this.state.newPassword || !this.state.confirmPassword) {
      alert('Please all fields');
    }
    if (this.state.newPassword !== this.state.confirmPassword) {
      alert('Passwords do not match');
    } else {
      try {
        let otpVerifyToken = localStorage.getItem('otpVerifyToken')
        let data = {
          newPassword: this.state.newPassword,
          otpVerifyToken: otpVerifyToken,
        }
        fetch('http://93.188.167.68:3001/api/' + `users/forgotPassword`, {
          method: 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data)
        }).then(response =>
          response.json()).then(response => {
            if (response.isSuccess == true) {
              alert(response.message);
              localStorage.removeItem('otpVerifyToken')
              hashHistory.push('/auth/login-page')
            } else {
              alert(response.error);
            }
          }).catch(error => {
            console.log('Errorrrrrr:', error)
            alert("Please Check your internet Connection ")
            this.setState({
              isLoading: false
            })
          })
      } catch (error) {
        console.log('error', error)
      }
    }


  }
  render() {
    return (
      <Grid>
        <Row>
          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <form style={{ marginTop: "35%" }}>
              <Card
                hidden={this.state.cardHidden}
                textCenter
                content={
                  <div>
                    <h4 style={{ color: '#636FA4', fontSize: 21, fontWeight: 'bold', textAlign: "center" }}>Set Password</h4>
                    <FormGroup>
                      <ControlLabel>New Password</ControlLabel>
                      <FormControl placeholder="New Password" type="password" autoComplete="off" onChange={event => this.setState({ newPassword: event.target.value })}
                      />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Confirm Password</ControlLabel>
                      <FormControl placeholder="Confirm Password" type="password" autoComplete="off" onChange={event => this.setState({ confirmPassword: event.target.value })}
                      />
                    </FormGroup>
                  </div>
                }

                legend={
                  <Button style={{ backgroundColor: '#636FA4', borderColor: "#FFF", marginBottom: "10%" }} onClick={this.setPassword} bsStyle="info" fill wd>
                    SUBMIT
                  </Button>
                }
                ftTextCenter
              />
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default ForgotPasswordPage;
