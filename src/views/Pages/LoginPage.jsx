import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";
import Card from "components/Card/Card.jsx";

import Button from "components/CustomButton/CustomButton.jsx";
import { hashHistory } from 'react-router';

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      cardHidden: true
    };
  }
  componentDidMount() {
    setTimeout(
      function () {
        this.setState({ cardHidden: false });
      }.bind(this),
      700
    );
  }
  forgotPassword() {
    hashHistory.push('/auth/otp-page')
  }
  login = () => {
    console.log('detail', this.state)

    if (this.state.email === '' || this.state.password === '') {
      return alert('All fields are required')
    }
    var pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!pattern.test(String(this.state.email).toLowerCase())) {
      return alert("Please Enter Valid Email");
    }
    else {

      let data = {
        email: this.state.email,
        password: this.state.password,
      }

      const userDetail = JSON.stringify(data)

      fetch('http://93.188.167.68:3001/api/' + 'users/login', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: userDetail
      }).then(response =>
        response.json()).then(response => {
          console.log("SignUp", response)
          if (response.isSuccess == true) {
            console.log("Login:response", response)
            localStorage.setItem('token', response.data.token)
            hashHistory.push('/admin/dashboard')
          } else {
            alert(response.error);
          }
        }).catch(error => {
          console.log('Errorrrrrr:', error)
          alert("Please Check your internet Connection ")
          this.setState({
            isLoading: false
          })
        })
    }

  }
  render() {
    return (
      <Grid>
        <Row>
          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <form>
              <Card
                hidden={this.state.cardHidden}
                textCenter
                content={
                  <div>
                    <h4 style={{ color: '#636FA4', fontSize: 30, fontWeight: 'bold', textAlign: "center" }}>Login</h4>
                    <FormGroup>
                      <ControlLabel>Email address</ControlLabel>
                      <FormControl placeholder="Enter email" type="email" onChange={event => this.setState({ email: event.target.value })} />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Password</ControlLabel>
                      <FormControl placeholder="Password" type="password" autoComplete="off" onChange={event => this.setState({ password: event.target.value })}
                      />
                    </FormGroup>

                  </div>
                }

                legend={
                  <div>
                    <Button style={{ backgroundColor: '#636FA4', borderColor: "#FFF" }} onClick={this.login} bsStyle="info" fill wd>
                      Login
                  </Button>
                    {/* <NavLink to={"/auth/otp-page"} style={{ border: "none", marginLeft: "60%", color: "#636FA4", fontWeight: "400" }} className="nav-link">
                      <p>Forgot Password</p>
                    </NavLink> */}
                    <Button variant="link" style={{ border: "none", marginLeft: "60%", color: "#636FA4", fontWeight: "bold", textDecoration: "underline" }}
                      onClick={this.forgotPassword}>Forgot Password</Button>
                  </div>
                }
                ftTextCenter
              />
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default LoginPage;
