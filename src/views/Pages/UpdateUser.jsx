import React, { Component } from "react";
import { Grid, Row, Col, Media, FormControl, ControlLabel, FormGroup } from "react-bootstrap";

import Card from "components/Card/Card.jsx";

import Button from "components/CustomButton/CustomButton.jsx";
import { hashHistory } from 'react-router';

class RegisterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      firstName: '',
      lastName: '',
      id: '',
      Mobile: '',
      isDriver: '',
      cardHidden: true
    };
  }

  Cancel() {
    // hashHistory.push('/auth/forgot-password')
    hashHistory.push('/admin/users')
  }
  Update() {
    alert("hi")
  }
  componentDidMount() {
    const data = this.props.location && this.props.location.state ? this.props.location.state.data : null
    if (data) {
      this.setState({
        email: data.email,
        firstName: data.firstName,
        lastName: data.lastName,
        id: data.id,
        Mobile: data.Mobile,
        isDriver: data.isDriver
      })
    }

    console.log("dataaa", data)
  }
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  render() {
    return (
      <Grid>
        <Row>
          {/* <Col md={9} mdOffset={4}> */}
          <Col md={2} sm={8} mdOffset={4} smOffset={4}>
            <div className="header-text">
              <h2>Edit User</h2>
              <hr />
            </div>
          </Col>
          <Col md={11}>
            <form>
              <Card
                plain
                content={
                  <div>
                    <FormGroup>
                      <ControlLabel>First Name</ControlLabel>
                      {/* this.setState({ [e.target.name]: e.target.value }); */}
                      <FormControl type="text" name="firstName" placeholder="firstName" onChange={this.handleChange} value={this.state.firstName} />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Last Name</ControlLabel>
                      <FormControl type="text" name="lastName" placeholder="lastName" onChange={this.handleChange} value={this.state.lastName} />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Email</ControlLabel>
                      <FormControl type="text" name="Email" placeholder="Email" onChange={this.handleChange} value={this.state.email} />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Mobile No.</ControlLabel>
                      <FormControl type="text" name="Mobile" placeholder="Mobile" onChange={this.handleChange} value={this.state.mobile} />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Driver/Rider</ControlLabel>
                      <FormControl type="email" name="isDriver" placeholder="Driver/Rider" onChange={this.handleChange} value={this.state.isDriver} />
                    </FormGroup>
                  </div>
                }
                ftTextCenter
                legend={
                  <div>
                    <Button style={{ backgroundColor: '#636FA4', borderColor: "#FFF", marginBottom: "10%" }} onClick={this.Cancel} bsStyle="info" fill wd>
                      Cancel
                </Button>
                    <Button style={{ backgroundColor: '#636FA4', borderColor: "#FFF", marginBottom: "10%" }} onClick={this.Update} bsStyle="info" fill wd>
                      Save
                  </Button>
                  </div>
                }
              />
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default RegisterPage;
