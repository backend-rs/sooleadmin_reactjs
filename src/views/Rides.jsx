import React, { Component } from "react";
import ReactTable from "react-table";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
// import Loader from 'react-loader-spinner'
import LoadingOverlay from 'react-loading-overlay';
class Rides extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataTable: [],
      data: []
    };
  }

  update(dataTable) {
    this.setState({
      isLoading: false,
      data: dataTable.map((prop, key) => {
        return {
          isLoading: false,
          id: prop[0],
          name: prop[1],
          mobile: prop[4],
          origin_addresses: prop[2],
          destination_addresses: prop[3],
          paymentMode: prop[5],
          seats: prop[6],
          amount: prop[7],
          driver: prop[10],
          actions: (
            // we've added some custom button actions
            <div className="actions-right">
              {/* use this button to add a edit kind of action */}
              {/* <Button
                onClick={() => {
                  let obj = this.state.data.find(o => o.id === key);
                  alert(
                    "You've clicked EDIT button on \n{ \Id: " +
                    obj.id +
                    ", \nName: " +
                    obj.name +
                    ", \nEmail: " +
                    obj.email +
                    ", \nPhone no.: " +
                    obj.mobile +
                    ", \Driver/Rider: " +
                    obj.isDriver +
                    "\n}."
                  );
                }}
                bsStyle="warning"
                simple
                icon
              >
                <i className="fa fa-edit" />
              </Button>{" "} */}
              {/* use this button to remove the data row */}
              {/* <Button
                onClick={() => {
                  var data = this.state.data;
                  data.find((o, i) => {
                    if (o.id === key) {
                      // here you should add some custom code so you can delete the data
                      // from this component and from your server as well
                      data.splice(i, 1);
                      console.log(data);
                      return true;
                    }
                    return false;
                  });
                  this.setState({ data: data });
                }}
                bsStyle="danger"
                simple
                icon
              >
                <i className="pe-7s-delete-user" />
              </Button>{" "} */}
            </div>
          )
        };
      })
    })
  }
  componentDidMount() {
    this.setState({
      isLoading: true
    })
    let token = localStorage.getItem('token')
    console.log('token', token)
    fetch('http://93.188.167.68:3001/api/' + 'rides', {
      method: 'get',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': token
      },
    }).then(response =>
      response.json()).then(response => {
        // console.log("countsApi", response)
        if (response.isSuccess == true) {
          // console.log("countsApi:data", response.data.count)
          this.setState({
            dataTable: response.data,
            isLoading: false
          })
          this.update(response.data)
        } else {
          alert(response.error);
        }
      }).catch(error => {
        console.log('Errorrrrrr:', error.message)
        alert(error.message)
        this.setState({
          isLoading: false
        })
      })
  }
  render() {
    console.log("dataTable", this.state.dataTable)

    return (
      <LoadingOverlay
        active={this.state.isLoading}
        spinner
        text='Loading, please wait...'
      >
        <div className="main-content">
          <Card
            // title="DataTables.net"
            content={
              <ReactTable
                data={this.state.data}
                filterable

                columns={[
                  {
                    Header: "Id",
                    accessor: "id"
                  },
                  {
                    Header: "Name",
                    accessor: "name"
                  },
                  {
                    Header: "Phone no.",
                    accessor: "mobile"
                  },
                  {
                    Header: "Origin",
                    accessor: "origin_addresses"
                  },
                  {
                    Header: "Destination",
                    accessor: "destination_addresses"
                  },
                  {
                    Header: "Payment",
                    accessor: "paymentMode"
                  },
                  {
                    Header: "Amount",
                    accessor: "amount"
                  },
                  {
                    Header: "Booked Seats",
                    accessor: "seats"
                  },
                  {
                    Header: "Driver",
                    accessor: "driver"
                  },
                  {
                    Header: "Ride At",
                    accessor: "rideAt"
                  }
                ]}
                defaultPageSize={5}
                showPaginationTop
                showPaginationBottom={false}
                className="-striped -highlight"
              />
            }
          />
        </div>
      </LoadingOverlay>
    );
    // } else {
    // return (
    //   <div className="main-content">
    //     <Loader
    //       type="Oval"
    //       color="#00BFFF"
    //       height="100"
    //       width="100"
    //     />
    //   </div>
    // );
    // }
  }

}

export default Rides;
