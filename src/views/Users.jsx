import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import ReactTable from "react-table";
import Card from "components/Card/Card.jsx";
// react component used to create charts
import Button from "components/CustomButton/CustomButton.jsx";
// import Loader from 'react-loader-spinner'
import LoadingOverlay from 'react-loading-overlay';


class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataTable: [],
      data: []
    };
    // this.update(dataTable)
  }

  update(dataTable) {
    this.setState({
      isLoading: false,
      data: dataTable.map((prop, key) => {
        return {
          isLoading: false,
          id: prop[0],
          name: prop[1],
          email: prop[2],
          mobile: prop[3],
          isDriver: prop[4] ? 'Driver' : 'Rider',
          firstName: prop[5],
          lastName: prop[6],
          actions: (
            // we've added some custom button actions
            <div className="actions-right">
              {/* use this button to add a edit kind of action */}
              <Button
                onClick={() => {

                  let obj = this.state.data;

                  obj.find((item, i) => {
                    if (i === key) {
                      const { history } = this.props;
                      history.push({ pathname: '/admin/edit-user', state: { data: item } })
                      // alert(
                      //   "You've clicked EDIT button on \n{ \Id: " +
                      //   item.id +
                      //   ", \nName: " +
                      //   item.name +
                      //   ", \nEmail: " +
                      //   item.email +
                      //   ", \nPhone no.: " +
                      //   item.mobile +
                      //   ", \nProfile: " +
                      //   item.isDriver +
                      //   "\n}."
                      // );
                    }
                  })

                }}
                bsStyle="warning"
                simple
                icon
              >
                <i className="fa fa-edit" />
              </Button>{" "}
              {/* use this button to remove the data row */}
              <Button
                onClick={() => {
                  var data = this.state.data;
                  data.find((item, i) => {
                    if (i === key) {
                      let token = localStorage.getItem('token')
                      // console.log('token', token)
                      fetch('http://93.188.167.68:3001/api/' + `users/delete/${item.id}`, {
                        method: 'PUT',
                        headers: {
                          Accept: 'application/json',
                          'Content-Type': 'application/json',
                          'x-access-token': token
                        },
                      }).then(response =>
                        response.json()).then(response => {
                          if (response.isSuccess == true) {
                            this.getUsers()
                            return true;
                          } else {
                            alert(response.error);
                          }
                        }).catch(error => {
                          console.log('Errorrrrrr:', error.message)
                          alert(error.message)
                          this.setState({
                            isLoading: false
                          })
                        })
                    }
                    return false;
                  });
                  this.setState({ data: data });
                }}
                bsStyle="danger"
                simple
                icon
              >
                <i className="pe-7s-delete-user" />
              </Button>{" "}
            </div>
          )
        };
      })
    })
  }
  componentDidMount() {
    this.setState({
      isLoading: true
    })
    this.getUsers()
  }
  getUsers() {

    let token = localStorage.getItem('token')
    // console.log('token', token)
    fetch('http://93.188.167.68:3001/api/' + 'users/getList', {
      method: 'get',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': token
      },
    }).then(response =>
      response.json()).then(response => {
        // console.log("countsApi", response)
        if (response.isSuccess == true) {
          // console.log("countsApi:data", response.data.count)
          this.setState({
            dataTable: response.data,
            isLoading: false
          })
          this.update(response.data)
        } else {
          alert(response.error);
        }
      }).catch(error => {
        console.log('Errorrrrrr:', error.message)
        alert(error.message)
        this.setState({
          isLoading: false
        })
      })
  }
  render() {
    // console.log("dataTable", this.state.dataTable)

    return (
      <LoadingOverlay
        active={this.state.isLoading}
        spinner
        text='Loading, please wait...'
      >
        <div className="main-content">
          <Card
            // title="DataTables.net"
            content={
              <ReactTable
                data={this.state.data}
                filterable
                columns={[
                  {
                    Header: "Id",
                    accessor: "id"
                  },
                  {
                    Header: "Name",
                    accessor: "name"
                  },
                  {
                    Header: "Email",
                    accessor: "email"
                  },
                  {
                    Header: "Phone no.",
                    accessor: "mobile"
                  },
                  {
                    Header: "Driver/Rider",
                    accessor: "isDriver"
                  },

                  {
                    Header: "Actions",
                    accessor: "actions",
                    sortable: false,
                    filterable: false
                  }
                ]}
                defaultPageSize={10}
                showPaginationTop
                showPaginationBottom={false}
                className="-striped -highlight"
              />
            }
          />
        </div>
      </LoadingOverlay>
    );
    // } else {
    // return (
    //   <div className="main-content">
    //     <Loader
    //       type="Oval"
    //       color="#00BFFF"
    //       height="100"
    //       width="100"
    //     />
    //   </div>
    // );
    // }
  }

}

export default Users;
